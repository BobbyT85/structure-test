FT.manifest({
	"filename":"index.html",
	"width":300,
	"height":250,
	"clickTagCount":1,
	"hideBrowsers": ["ie8"],
	"instantAds":[
		{"name":"homeTeamlrg", "type":"image", "default":"images/300x250bournemouth-lrg-home.png"},
		{"name":"awayTeamlrg", "type":"image", "default":"images/300x250southampton-lrg-away.png"},

		{"name":"homeTeamOddsTab", "type":"text", "default":"#b5112c"},
		{"name":"homeTeamoddstabname", "type":"text", "default":"BOU"},
		{"name":"homeTeamoddsprice", "type":"text", "default":"8/1"},

		{"name":"drawoddstabname", "type":"text", "default":"DRAW"},
		{"name":"drawoddsprice", "type":"text", "default":"6/1"},

		{"name":"awayTeamOddsTab", "type":"text", "default":"#000000"},
		{"name":"awayTeamoddstabname", "type":"text", "default":"<span style='color:#b5112c'>SOU</span>"},
		{"name":"awayTeamoddsprice", "type":"text", "default":"4/1"},
		
		{"name":"offercopy1", "type":"image", "default":"images/offercopy1.png"},
		{"name":"offercopy2", "type":"image", "default":"images/offercopy2.png"},
		{"name":"offercopy3", "type":"image", "default":"images/offercopy3.png"},

		{"name":"mainLegals", "type":"text", "default":"Min odds 2/1 (3.00). Single bets only. First<br>eligble bet placed. Bets placed with Free Bets<br>ineligible. Free Bet stake not included in<br>winnings. Full terms apply 18+."},
		{"name":"gambleawareURLcopy", "type":"text", "default":"18+ gambleaware.org"}
	]
});