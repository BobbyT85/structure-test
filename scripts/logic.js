// --------------------------------------------------------------------------------------------------
// SETUP VARIABLES
var standalone = true,
	bannerHeight = 250,
	bannerWidth = 300,
	tl = new TimelineLite({paused:true});





// --------------------------------------------------------------------------------------------------
// INITIALISE
domready(function() {
	console.log(":: dom ready ::");
	if (elementScrape()) init();
});

function init() {
	setTimeout(setupElements, 500);
}

function setupElements() {	
	// console.log(homeTeamOddsCont);
	document.getElementById('homeTeamOddsTabColor').style.fill = homeTeamOddsTab.innerHTML;

	// console.log(awayTeamOddsCont);
	document.getElementById('awayTeamOddsTabColor').style.fill = awayTeamOddsTab.innerHTML;

	anim();
}

function delayedStart() {
	console.log('Banner Started');
	console.log('standalone: ' + standalone);
	if (standalone) tl.play();
}





function anim() {
	tl.addLabel('start', 0)	
	.set(swoosh, {clip:'rect('+bannerHeight+'px,'+bannerWidth+'px,'+bannerHeight+'px,0px)'})

	tl.to(preloadShape, 0.5, {autoAlpha:0}, 'start')

	 .from(home_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'start+=0.5')
	 .from(away_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'start+=0.7')
	

	tl.addLabel('frame2', 2.3)
	.from(cta, 0.5, {x:bannerWidth, ease:Expo.easeInOut}, 'frame2')

	.to(home_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'frame2')
	.to(away_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'frame2+=0.2')

	.from(homeTeamOddsCont, 0.5, {x:10, autoAlpha:0}, 'frame2+=0.7')
	.from(drawOddsCont, 0.5, {autoAlpha:0}, 'frame2+=0.7')
	.from(awayTeamOddsCont, 0.5, {x:-10, autoAlpha:0}, 'frame2+=0.7')

	
	tl.addLabel('frame3', 5)
	.to(swoosh, 0.8, {clip:'rect(0px,'+bannerWidth+'px,'+bannerHeight+'px,0px)', ease:Expo.easeInOut}, 'frame3')
	.set(homeTeamOddsCont, {x:10, autoAlpha:0}, 'frame3+=0.8')
	.set(drawOddsCont, {autoAlpha:0}, 'frame3+=0.8')
	.set(awayTeamOddsCont, {x:-10, autoAlpha:0}, 'frame3+=0.8')

	.from(offer1, 0.5, {x:-bannerWidth, ease: Expo.easeOut}, 'frame3+=0.4')
	.from(offer2, 0.5, {x:-bannerWidth, ease: Expo.easeOut}, 'frame3+=0.7')
	.from(offer3, 0.5, {x:-bannerWidth, ease: Expo.easeOut}, 'frame3+=1.0')


	tl.addLabel('frame4', 7.5)
	.to([offer1, offer2, offer3], 0.5, {autoAlpha:0}, 'frame4')

	.from([main_legal, gamble_logo, gamble_URL] , 0.5, {autoAlpha:0}, 'frame4+=0.5')

	.to([main_legal, gamble_logo, gamble_URL] , 0.5, {autoAlpha:0}, 'frame4+=3.0')

	.to(swoosh, 0.8, {clip:'rect('+bannerHeight+'px,'+bannerWidth+'px,'+bannerHeight+'px,0px)', ease:Expo.easeInOut}, 'frame4+=3.0')


	tl.addLabel('endframe', 10.5)	
	.from(end_A, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=0.4')
    .from(end_B, 0.5, {x:bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=0.6')
	.from(end_C, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=0.8')
	   
	.to(end_A, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=2.0')
    .to(end_B, 0.5, {x:bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=2.2')
    .to(end_C, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=2.4')
	   
	.to(homeTeamOddsCont, 0.5, {x:0, autoAlpha:1}, 'endframe+=2.9')
    .to(drawOddsCont, 0.5, {autoAlpha:1}, 'endframe+=2.9')
    .to(awayTeamOddsCont, 0.5, {x:0, autoAlpha:1}, 'endframe+=2.9')


	setTimeout(delayedStart, 100);
}





// --------------------------------------------------------------------------------------------------
// EVENT LISTENERS
function addListeners() {
	console.log('[Added] Event Listeners');

	mainContainer.addEventListener('click', exitClickHandler);
}





// --------------------------------------------------------------------------------------------------
// EVENT HANDLING
function exitClickHandler() {
	myFT.on("instantads", function() {
		myFT.applyClickTag(myFT.$("body"), 1);
	});
	console.log('BackgroundExit');
}